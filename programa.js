class Mapa{

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligonos=[];

    constructor(){

        this.posicionInicial=[5.876265,-72.983249];
        this.escalaInicial=14;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor={
            maxZoom:20
        };

        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial,this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL,this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);

    }

    colocarMarcador(posicion){

        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor);
    }

    colocarCirculo(posicion, configuracion){

        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor);
    }

    colocarPoligono(posicion, configuracion){

        this.poligonos.push(L.polygon(posicion, configuracion));
        this.poligonos[this.poligonos.length-1].addTo(this.miVisor);
    }
    
}


let miMapa=new Mapa();

miMapa.colocarMarcador([5.876265,-72.983249]);
miMapa.colocarMarcador([4.6280361,-74.065661]);

miMapa.colocarCirculo([5.876265,-72.983249], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500

});

miMapa.colocarCirculo([4.6280361,-74.065661], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500

});

miMapa.colocarPoligono([[5.8735639,-72.984011],[5.8765556,-72.983103],[5.8759222,-72.980481],[5.8727667,-72.981383]], {
    color: 'yellow',
    fillColor: 'yellow',
    fillOpacity: 0.5,
});